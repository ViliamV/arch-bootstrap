#!/bin/sh

DOTFILES_LOCATION="$HOME/dotfiles"
[ $(uname) == Linux ] && LINUX=1 || LINUX=

echo "Welcome to bootstrap!"
echo

if [ $LINUX ]; then
  # AUR
  export AUR=paru
  if ! type $AUR >/dev/null 2>&1; then
    echo "-> Installing AUR helper $AUR"
    git clone https://aur.archlinux.org/paru.git
    cd paru || exit
    makepkg -si
    cd ..
    rm -rf paru
    paru -S paru-bin
  fi

  ${AUR} -S --needed udisks2 chezmoi
else
  if ! type brew >/dev/null 2>&1; then
    echo "-> Installing brew"
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
  fi

  brew install gnupg chezmoi fd
fi

# GPG setup
echo "-> GPG setup:"
echo ""
echo "On another pc mount USB drive, cd there and run:"
echo "gpg --export-secret-keys --armor --output deleteme.asc <USER_ID>"
echo "gpg --export-ownertrust > ownertrust.txt"
echo ""
echo "Connect drive to this computer and press ENTER..."
read -r _
echo "Connected drives:"
[ $LINUX ] && lsblk || diskutil list external
printf "USB drive name (e.g. /dev/sda1)?: "
read -r dev
[ -z "$dev" ] && exit 1
if [ $LINUX ]; then
  udisksctl mount -b "$dev" || exit 1
  mountpoint=$(lsblk --raw -o NAME,MOUNTPOINT | grep "$dev" | cut -f2 -d' ')
else
  diskutil mountDisk "$dev" || exit 1
  mountpoint=/Volumes/Untitled
fi

[ -z "$mountpoint" ] && exit 1

# TODO: pinentry program for mac
gpg --import "$mountpoint/deleteme.asc" && rm "$mountpoint/deleteme.asc"
gpg --import-ownertrust "$mountpoint/ownertrust.txt" && rm "$mountpoint/ownertrust.txt"
if [ $LINUX ]; then
  udisksctl unmount -b "$dev" || exit 1
fi

# Dotfiles
echo "-> Installing dotfiles"
if [ ! -d "$DOTFILES_LOCATION" ]; then
  printf "Are you in TTY (no GUI/over SSH)? y/n: "
  read -r tty
  [ "$tty" = y ] && export GPG_TTY=$(tty)
  # TODO: mac does not have pip by default
  chezmoi init --source="$DOTFILES_LOCATION" --apply https://gitlab.com/ViliamV/chezmoi-dotfiles
  chezmoi git remote set-url origin git@gitlab.com:ViliamV/chezmoi-dotfiles.git
fi

echo "All done!"
echo "Log out or restart"
