# arch-bootstrap

simple script for bootstrapping dotfiles on Arch linux

## Installation

```sh
sh -c "$(curl -fsLS script.viliamvalent.com/bootstrap)"
```
